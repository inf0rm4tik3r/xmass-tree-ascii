# Xmas Tree ASCII generator

              *
              X
             XXX
            XXXXX
           XXXXXXX
          XXXXXXXXX
         XXXXXXXXXXX
        XXXXXXXXXXXXX
              |
              |

This is an example implementation of the christmas tree Kata, using Rust.
The tree-string is produced lazily by using iterators only.

Compile the program with

        cargo build --release

Run the program using the following command-line arguments

- --star (or -s):  for adding the star on top
- --branches (or -b) for specifying the amount of tree-branches
- --stem-length (or -sl) for specifying the length of the stem

The above output was generated using the following command

        xmas-tree-ascii.exe -star -branches=6 -stem-length=2

or alternatively

        xmas-tree-ascii.exe -s -b 6 -sl 2
