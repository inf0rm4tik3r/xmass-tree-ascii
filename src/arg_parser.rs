#[derive(Debug)]
pub struct SupportedArg {
    pub long_name: &'static str,
    pub short_name: Option<&'static str>,
    pub needs_value: bool,
}

#[derive(Debug)]
pub struct Argument {
    pub name: &'static str,
    pub value: Option<String>,
}

struct ArgMatcher<const SIZE: usize> {
    supported_args: [SupportedArg; SIZE],
}

impl<const SIZE: usize> ArgMatcher<SIZE> {
    pub fn new(supported_args: [SupportedArg; SIZE]) -> Self {
        Self { supported_args }
    }

    pub fn match_arg(&self, arg: &str) -> Option<&SupportedArg> {
        let mut arg = arg.trim().trim_start_matches("-");
        if let Some((first, _)) = arg.split_once("=") {
            arg = first;
        }
        self.supported_args.iter().find(|supported_arg| {
            supported_arg.short_name.is_some() && supported_arg.short_name.unwrap() == arg
                || supported_arg.long_name == arg
        })
    }

    pub fn extract_value<'a>(&self, arg: &'a str) -> Option<&'a str> {
        arg.trim().split_once("=").map(|(_, value)| value)
    }
}

pub struct ArgParser<I: Iterator<Item = String>, const SIZE: usize> {
    matcher: ArgMatcher<SIZE>,
    args: I,
}

impl<I: Iterator<Item = String>, const SIZE: usize> ArgParser<I, SIZE> {
    pub fn new(supported_args: [SupportedArg; SIZE], args: I) -> Self {
        Self {
            matcher: ArgMatcher::new(supported_args),
            args,
        }
    }
}

impl<I: Iterator<Item = String>, const SIZE: usize> Iterator for ArgParser<I, SIZE> {
    type Item = Argument;

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(arg) = self.args.next() {
            match self.matcher.match_arg(arg.as_str()) {
                Some(supported_arg) => {
                    if supported_arg.needs_value {
                        if let Some(contained_value) = self.matcher.extract_value(arg.as_str()) {
                            return Some(Argument {
                                name: supported_arg.long_name,
                                value: Some(contained_value.to_string()),
                            });
                        }
                        return Some(Argument {
                            name: supported_arg.long_name,
                            value: self.args.next(),
                        });
                    }
                    return Some(Argument {
                        name: supported_arg.long_name,
                        value: None,
                    });
                }
                None => {
                    println!("WARN: Unable to match program argument \"{}\" to any known argument. Skipping it.", arg);
                    return self.next();
                }
            }
        }
        None
    }
}
