mod arg_parser;

use std::iter::{once, repeat};

use crate::arg_parser::{ArgParser, SupportedArg};

const LINE_BREAK: char = '\n';
const SPACE: char = ' ';
const STAR: char = '*';
const TREE: char = 'X';
const STEM: char = '|';

struct XmasTree {
    with_star: bool,
    branches: usize,
    stem_length: usize,
}

impl XmasTree {
    pub fn as_ascii_drawing(&self) -> impl Iterator<Item = char> + '_ {
        let center: usize = self.max_width() / 2;
        self.star(center)
            .chain(self.branches())
            .chain(self.stem(center))
    }

    fn max_width(&self) -> usize {
        2 * self.branches - 1
    }

    fn star(&self, offset: usize) -> impl Iterator<Item = char> {
        repeat(
            repeat(SPACE)
                .take(offset)
                .chain(once(STAR))
                .chain(once(LINE_BREAK)),
        )
        .take(if self.with_star { 1 } else { 0 })
        .flatten()
    }

    fn branches(&self) -> impl Iterator<Item = char> + '_ {
        (0..self.branches).into_iter().flat_map(|i| self.branch(i))
    }

    fn branch(&self, i: usize) -> impl Iterator<Item = char> {
        repeat(SPACE)
            .take(self.branches - i - 1)
            .chain(repeat(TREE).take(2 * i + 1))
            .chain(once(LINE_BREAK))
    }

    fn stem(&self, offset: usize) -> impl Iterator<Item = char> {
        repeat(
            repeat(SPACE)
                .take(offset)
                .chain(once(STEM))
                .chain(once(LINE_BREAK)),
        )
        .take(self.stem_length)
        .flatten()
    }
}

fn main() {
    let args = ArgParser::new(
        [
            SupportedArg {
                long_name: "with-star",
                short_name: Some("s"),
                needs_value: false,
            },
            SupportedArg {
                long_name: "branches",
                short_name: Some("b"),
                needs_value: true,
            },
            SupportedArg {
                long_name: "stem-length",
                short_name: Some("sl"),
                needs_value: true,
            },
        ],
        std::env::args().skip(1),
    );

    let mut with_star = false;
    let mut branches = 5;
    let mut stem_length = 1;

    for arg in args {
        match arg.name {
            "with-star" => with_star = true,
            "branches" => branches = arg.value.unwrap().parse::<usize>().unwrap(),
            "stem-length" => stem_length = arg.value.unwrap().parse::<usize>().unwrap(),
            _ => panic!("Unknown argument..."),
        }
    }

    let tree = XmasTree {
        with_star,
        branches,
        stem_length,
    };
    println!("{}", tree.as_ascii_drawing().collect::<String>());
}

#[test]
fn test_tree_of_size_7() {
    let tree = XmasTree {
        with_star: true,
        branches: 7,
        stem_length: 1,
    };
    assert_eq!(
        tree.as_ascii_drawing().collect::<String>(),
        [
            "      *",
            "      X",
            "     XXX",
            "    XXXXX",
            "   XXXXXXX",
            "  XXXXXXXXX",
            " XXXXXXXXXXX",
            "XXXXXXXXXXXXX",
            "      |",
            ""
        ]
        .join("\n")
    );
}

#[test]
fn test_tree_of_size_5() {
    let tree = XmasTree {
        with_star: true,
        branches: 5,
        stem_length: 1,
    };
    assert_eq!(
        tree.as_ascii_drawing().collect::<String>(),
        [
            "    *",
            "    X",
            "   XXX",
            "  XXXXX",
            " XXXXXXX",
            "XXXXXXXXX",
            "    |",
            ""
        ]
        .join("\n")
    );
}

#[test]
fn test_tree_without_star() {
    let tree = XmasTree {
        with_star: false,
        branches: 5,
        stem_length: 1,
    };
    assert_eq!(
        tree.as_ascii_drawing().collect::<String>(),
        [
            "    X",
            "   XXX",
            "  XXXXX",
            " XXXXXXX",
            "XXXXXXXXX",
            "    |",
            ""
        ]
        .join("\n")
    );
}

#[test]
fn test_tree_with_bigger_stem() {
    let tree = XmasTree {
        with_star: false,
        branches: 5,
        stem_length: 2,
    };
    assert_eq!(
        tree.as_ascii_drawing().collect::<String>(),
        [
            "    X",
            "   XXX",
            "  XXXXX",
            " XXXXXXX",
            "XXXXXXXXX",
            "    |",
            "    |",
            ""
        ]
        .join("\n")
    );
}
